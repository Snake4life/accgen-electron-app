var actualCode = `
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
function getvalue() {
    var url = new URL(document.referrer);
    window.parent.postMessage(document.getElementsByClassName("g-recaptcha-response")[0].value + ";" + "defunc", url.host.replace(/^[^.]+\./g, "") == "cathook.club" ? url.origin : "https://accgen.cathook.club"); //get recaptcha token and send to accgen
}
async function waitForRecap()
{
while(typeof grecaptcha == "undefined")
	await sleep(1000);
}
//wait for grecaptcha to be defined before calling rendering 
waitForRecap().then(async function next()
{
	grecaptcha.ready(function () {
		grecaptcha.render("g-recaptcha", {
			sitekey: "6LerFqAUAAAAABMeByEoQX9u10KRObjwHf66-eya",
			callback: getvalue
		}); //render recaptcha with params
	})
});

`;

function inIframe() {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

if (inIframe()) {
    //element to render recaptcha in
    document.getElementsByTagName("html")[0].innerHTML = '<div id="g-recaptcha" data-sitekey="6LerFqAUAAAAABMeByEoQX9u10KRObjwHf66-eya" data-callback="getvalue"></div>';
    var e = document.createElement("script");
    e.textContent = actualCode;
    (document.head || document.documentElement).appendChild(e);
}

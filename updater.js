const autoUpdater = require("electron-updater").autoUpdater;
const fs = require("fs");
const path = require("path");

var app = undefined;
var dataPath = undefined;
var ipcevent = undefined;

exports.default = function (_app, event) {
    app = _app;
    dataPath = app.getPath('userData');
    ipcevent = event;
    console.log(dataPath);
}

///////////////////
// Auto updater  //
///////////////////
autoUpdater.autoDownload = true;

autoUpdater.setFeedURL({
    provider: "generic",
    url: "https://gitlab.com/nullworks/accgen/accgen-electron-app/-/jobs/artifacts/master/raw/dist?job=build"
});

autoUpdater.on('checking-for-update', function () {
    sendStatusToWindow('Checking for update...');
});

autoUpdater.on('update-available', function (info) {
    sendStatusToWindow('Update available. Downloading!');
});

autoUpdater.on('update-not-available', function (info) {
    //sendStatusToWindow('Update not available.');
});

autoUpdater.on('error', function (err) {
    console.error(err)
    sendStatusToWindow('Auto updater error! Manually download here: https://gitlab.com/nullworks/accgen/accgen-electron-app/-/jobs/artifacts/master/browse/dist?job=build');
});

autoUpdater.on('download-progress', function (progressObj) {
    let log_message = 'Downloading update - Downloaded ' + parseInt(progressObj.percent) + '%';
    sendStatusToWindow(log_message);
});

autoUpdater.on('update-downloaded', function (info) {
    //sendStatusToWindow('Update downloaded; will install in 1 seconds');
    if (app && dataPath) {
        if (fs.existsSync(path.join(dataPath, "latest_update.txt"))) {
            var ver = fs.readFileSync(path.join(dataPath, "latest_update.txt"))
            if (ver == info.version) {
                sendStatusToWindow("Last update failed! Manually download here: https://gitlab.com/nullworks/accgen/accgen-electron-app/-/jobs/artifacts/master/browse/dist?job=build")
                return;
            } else
                fs.writeFileSync(path.join(dataPath, "latest_update.txt"), info.version)
        } else
            fs.writeFileSync(path.join(dataPath, "latest_update.txt"), info.version)
    }
    sendStatusToWindow("Update downloaded! Will be installed next time you restart the app!");
    setTimeout(() => {
        sendStatusToWindow(undefined);
    }, 30000);
});

autoUpdater.checkForUpdates();

function sendStatusToWindow(message) {
    if (message)
        console.log(message);
    if (ipcevent)
        ipcevent.reply('accgen-web-alert-msg', {
            status: message,
            electron: true
        });
}